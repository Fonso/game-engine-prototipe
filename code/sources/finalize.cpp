/*
    Started by Alfonso López Viejo on dicember of 2019
    Author: Alfonso López Viejo
    Copyright © 2020 Alfonso López Viejo
    Last Update: January/2020
    email: alfonsolopezviejo@gmail.com
*/

#include <SDL.h>

namespace engine
{

    /** Esta función se llamará automáticamente al salir de la función main() solo
      * si se inicializa algún subsistema de SDL.
      */
    void finalize ()
    {
        SDL_Quit ();
    }

}
