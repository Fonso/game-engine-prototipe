/*
    Started by Alfonso López Viejo on dicember of 2019
    Author: Alfonso López Viejo
    Copyright © 2020 Alfonso López Viejo
    Last Update: January/2020
    email: alfonsolopezviejo@gmail.com
*/

#include <SDL.h>
#include <cassert>
#include <OpenGL.hpp>
#include <iostream>
#include <internal/initialize.hpp>
#include <InputHandler.hpp>
#include <Mouse.hpp>
#include <Window.hpp>

namespace engine
{
    static Window::Event* m_event;

    Window::Window(const std::string & title, int width, int height, bool fullscreen)
    {
        m_event    = new Event(); 
        window     = nullptr;
        gl_context = nullptr; 
		
        if (initialize (SDL_INIT_VIDEO))
        {
            SDL_GL_SetAttribute (SDL_GL_CONTEXT_MAJOR_VERSION, 3);
            SDL_GL_SetAttribute (SDL_GL_CONTEXT_MINOR_VERSION, 2);

            window = SDL_CreateWindow
            (
                title.c_str (),
                SDL_WINDOWPOS_UNDEFINED,
                SDL_WINDOWPOS_UNDEFINED,
                width,
                height,
                SDL_WINDOW_OPENGL | SDL_WINDOW_SHOWN
            );
            assert(window != nullptr);

            if (window)
            {
                gl_context = SDL_GL_CreateContext (window);
                //Window::surface = SDL_GetWindowSurface(window); 
                assert(gl_context != nullptr);

                if (gl_context && glt::initialize_opengl_extensions ())
                {
                    if (fullscreen)
                    {
                        SDL_SetWindowFullscreen (window, SDL_WINDOW_FULLSCREEN_DESKTOP);
                    }
                }
            }
        }
    }

    Window::~Window()
    {
        if (gl_context) SDL_GL_DeleteContext (gl_context);
        if (window    ) SDL_DestroyWindow    (window    );
    }

    void Window::enable_vsync ()
    {
        if (gl_context) SDL_GL_SetSwapInterval (1);
    }

    void Window::disable_vsync ()
    {
        if (gl_context) SDL_GL_SetSwapInterval (0);
    }

    Window::Event* Window::get_event()
    {
        return m_event;
    }

    unsigned Window::get_width () const
    {
        int width = 0, height;

        if (window) SDL_GetWindowSize (window, &width, &height);

        return unsigned(width);
    }

    unsigned Window::get_height () const
    {
        int width, height = 0;

        if (window) SDL_GetWindowSize (window, &width, &height);

        return unsigned(height);
    }

    bool Window::poll (Event & event, InputHandler *input_handler) const
    {
        if (window)     // Aunque sería raro, puede llegar a ocurrir que no se haya conseguido crear la ventana...
        {
            // Se extrae un evento usando SDL y se convierte a un evento propio de
            // nuestro engine:   
            SDL_Event sdl_event;

            if (SDL_PollEvent (&sdl_event) > 0)
            {
                switch (sdl_event.type)
                {
					case SDL_MOUSEBUTTONDOWN: 
					{
						//event.data.mouse.buttons = SDL_GetMouseState(NULL, NULL) & SDL_BUTTON(SDL_BUTTON_LEFT); 
						//std::cout << "Mouse button down"; 
                        event.type = Window::Event::MOUSE_DOWN;
                        if(SDL_GetMouseState(NULL,NULL) & SDL_BUTTON(SDL_BUTTON_LEFT))
						{
                             
						
                            event.data.mouse.buttons = Mouse::translate_sdl_mouse_code(SDL_BUTTON_LEFT); 
							//std::cout << "Mouse button left";

						}
						else if(SDL_GetMouseState(NULL, NULL) & SDL_BUTTON(SDL_BUTTON_RIGHT))
						{
							event.data.mouse.buttons = Mouse::translate_sdl_mouse_code(SDL_BUTTON_RIGHT);
							//std::cout << "Mouse button right";
						}
						else
						{
							event.data.mouse.buttons = Mouse::translate_sdl_mouse_code(SDL_BUTTON_MIDDLE);
							//std::cout << "Mouse button middle";
						}
						break;
					}

					case SDL_MOUSEMOTION:
					{
                        event.type = Window::Event::MOUSE_MOTION; 
						SDL_GetMouseState(&event.data.mouse.x, &event.data.mouse.y);
						break; 
					}

					case SDL_MOUSEBUTTONUP:
					{
                        event.type = Window::Event::MOUSE_UP;
						break; 
					}

                    case SDL_QUIT:
                    {
                        event.type = Event::CLOSE;
                        break;
                    }

                    case SDL_KEYDOWN:
                    {
                        event.type = Event::KEY_PRESSED;
                        event.data.keyboard.key_code = Keyboard::translate_sdl_key_code (sdl_event.key.keysym.sym);
                        input_handler->enque_key((Keyboard::Key_Code)event.data.keyboard.key_code);
                        break;
                    }

                    case SDL_KEYUP:
                    {
                        event.type = Event::KEY_RELEASED;
                        event.data.keyboard.key_code = Keyboard::translate_sdl_key_code (sdl_event.key.keysym.sym);
                        break;
                    }
                }
				
                return true;
            }
        }

        return false;
    }

    void Window::clear () const
    {
        if (gl_context) glClear (GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    }

    void Window::swap_buffers () const
    {
        if (gl_context) SDL_GL_SwapWindow (window);
    }

    void Window::set_window_fullscreen() const
    {
        SDL_SetWindowFullscreen(window, SDL_WINDOW_FULLSCREEN_DESKTOP);
    }

    void Window::set_window_resizable() const
    {
        SDL_SetWindowResizable(window, SDL_TRUE);
    }

    void Window::set_window_bordered() const
    {
        SDL_SetWindowBordered(window, SDL_TRUE);
    }

    void Window::hide_window()const
    {
        SDL_HideWindow(window);
    }
    void Window::show_window() const
    {
        SDL_ShowWindow(window);
    }

    void Window::maximize_window() const
    {
        SDL_MaximizeWindow(window);
    }

    void Window::minimice_window() const
    {
        SDL_MinimizeWindow(window);
    }

    void Window::restore_window() const
    {
        SDL_RestoreWindow(window);
    }

    void Window::destroy_window() const
    {
        SDL_DestroyWindow(window);
    }

    void Window::set_window_position(int new_x, int new_y)
    {
        SDL_SetWindowPosition(window, new_x, new_y);
    }

    void Window::set_window_centered()
    {

        SDL_SetWindowPosition(window, SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED);
    }

    void Window::set_window_title(const char* new_title)
    {
        SDL_SetWindowTitle(window,new_title); 
    }

    
}
