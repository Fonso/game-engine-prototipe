/*
    Started by Alfonso L�pez Viejo on dicember of 2019
    Author: Alfonso L�pez Viejo
    Copyright � 2020 Alfonso L�pez Viejo
    Last Update: January/2020
    email: alfonsolopezviejo@gmail.com
*/

#include <SDL.h>
#include <Timer.hpp>

void engine::Timer::start()
{
	actual_ticks = start_ticks = SDL_GetTicks(); 
}

float engine::Timer::elapsed_seconds() const
{
	return float(elapsed_milliseconds()) / 1000.f;
}

uint32_t engine::Timer::elapsed_milliseconds() const
{
	return SDL_GetTicks() - start_ticks;
}
