/*
    Started by Alfonso López Viejo on dicember of 2019
    Author: Alfonso López Viejo
    Copyright © 2020 Alfonso López Viejo
    Last Update: January/2020
    email: alfonsolopezviejo@gmail.com  
*/ 
#include <Message.hpp>
#include <Observer.hpp>
#include <Dispatcher.hpp>
#include <Music.hpp>
#include <Sound.hpp>
#include <Task.hpp>
#include <Kernel.hpp>
#include <Scene.hpp>
#include <InputDevice.hpp>
#include <InputTask.hpp>
#include <RenderTask.hpp>
#include <UpdateTask.hpp>
#include <Window.hpp>
#include <iostream>
#include <Player.hpp>
#include <Enemy.hpp>

using namespace engine;

int main ()
{
	Music music(22048, 2, 4048);

    Sound sound(22048, 2, 4048);

    Window window("TEST", 1000, 1000, false);
	
	window.enable_vsync();

	window.set_window_resizable();

    string music_path, sound_path, scene_description_path, skull_path, mesh_path; 
    #ifdef _DEBUG
    music_path = "../../../Binaries/Assets/song.ogg";
    sound_path = "../../../Binaries/Assets/wall.wav"; 
    scene_description_path = "../../../Binaries/Assets/scene-description.xml"; 
    skull_path = "../../../Binaries/Assets/Skull.obj"; 
    mesh_path = "../../../Binaries/Assets/mesh.obj"; 
    #endif // DEBUG
    #ifdef NDEBUG 
    music_path = "Assets/song.ogg";
    sound_path = "Assets/wall.wav";
    scene_description_path = "Assets/scene-description.xml";
    skull_path = "Assets/Skull.obj";
    mesh_path = "Assets/mesh.obj";
    #endif

	music.load_and_play(music_path.c_str(), 0);

    sound.load_audio(sound_path.c_str());

	Scene main_scene(scene_description_path,window);
    
    //Aqui puedes hacer lo que quieras

    Entity *player = &main_scene.add_entity(0,    -30.f,  -20.f,
                                            180.f, 0,      0,
                                            0.2f,  0.2f,   0.2f,"Player");
    
    std::shared_ptr<Component> script_comp(new Player(player,&main_scene, 0.5f, &sound)); 
    player->add_component("Script", script_comp); 

    std::shared_ptr<Component> player_renderer(new Renderer_Component(skull_path));
    player->add_component("Renderer", player_renderer);


    Entity *enemy_1 = &main_scene.add_entity(-40.f, -35.f, -40.f,
                                              0,        0,     0,
                                              0.5f,  0.5f,  0.5f, "Enemy_1");

    std::shared_ptr<Component> enemy1_renderer(new Renderer_Component(mesh_path));
    enemy_1->add_component("Renderer", enemy1_renderer);

    std::shared_ptr<Component> enemy_script(new Enemy(enemy_1, 0.005f, player->get_transform(), &main_scene));
    enemy_1->add_component("Script", enemy_script);
    
    
    Entity* enemy_2 = &main_scene.add_entity(40.f, -35.f, -40.f,
                                             0,     0,     0,
                                             0.5f,  0.5f,  0.5f, "Enemy_2");

    std::shared_ptr<Component> enemy2_renderer(new Renderer_Component(mesh_path));
    enemy_2->add_component("Renderer", enemy2_renderer);

    std::shared_ptr<Component> enemy2_script(new Enemy(enemy_2, 0.005f, player->get_transform(), &main_scene));
    enemy_2->add_component("Script", enemy2_script);


    Entity* enemy_3 = &main_scene.add_entity(-40, -35.f,  0.f,
                                             0.f, 0, 0,
                                             0.5f, 0.5f, 0.5f, "Enemy_3");

    enemy_3->set_parent(*enemy_2); 

    std::shared_ptr<Component> enemy3_renderer(new Renderer_Component(mesh_path));
    enemy_3->add_component("Renderer", enemy3_renderer);

    std::shared_ptr<Component> enemy3_script(new Enemy(enemy_3, 0.005f, player->get_transform(), &main_scene));
    enemy_3->add_component("Script", enemy3_script);

    Entity* enemy_4 = &main_scene.add_entity(40.f,   -35.f,  0,
                                             0.f,     0,     0,
                                             0.5f,    0.5f,  0.5f, "Enemy_4");

    std::shared_ptr<Component> enemy4_renderer(new Renderer_Component(mesh_path));
    enemy_4->add_component("Renderer", enemy4_renderer);

    std::shared_ptr<Component> enemy4_script(new Enemy(enemy_4, 0.005f, player->get_transform(), &main_scene));
    enemy_4->add_component("Script", enemy4_script);


    main_scene.execute_scene(); 

    return 0;
}

