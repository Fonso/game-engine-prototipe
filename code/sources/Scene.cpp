/*
	Started by Alfonso L�pez Viejo on dicember of 2019
	Author: Alfonso L�pez Viejo
	Copyright � 2020 Alfonso L�pez Viejo
	Last Update: January/2020
	email: alfonsolopezviejo@gmail.com
*/

#include <rapidxml.hpp>
#include <sstream>
#include <iostream>
#include <vector>
#include <array>
#include <Scene.hpp>

using namespace rapidxml;
namespace engine 
{
	Scene::Scene(const string& scene_description_file_path, Window& w)
	{
		m_window = &w;
		root = &add_entity(0.f, 0.f, 0.f,
			0.f, 0.f, 0.f,
			1.f, 1.f, 1.f, "root");

		load(scene_description_file_path);

		init_kernel();
	}
	void Scene::load(const string& scene_description_file_path)
	{
		xml_document<> doc;
		xml_node<>* root_node;
		// Cargamos el XML
		ifstream theFile(scene_description_file_path);
		vector<char> buffer((istreambuf_iterator<char>(theFile)), istreambuf_iterator<char>());
		buffer.push_back('\0');
		// Parseamos el XML 
		doc.parse<0>(&buffer[0]);
		// Encontramos el nodo padre
		root_node = doc.first_node("scene");
		// Iteramos por los nodos buscando los elementos que nos interesan
		for (xml_node<>* entity_node = root_node->first_node("entity"); entity_node; entity_node = entity_node->next_sibling())
		{
			Entity* e = new Entity(entity_node->first_attribute("id")->value()); 
			Transform_Component* t = e->get_transform(); 
			Entity_Map.insert(std::make_pair(entity_node->first_attribute("id")->value(), e));
			e->set_parent(*root); 
			
			xml_node<>* components_node = entity_node->first_node("components"); 
			for (xml_node<>* component_node = components_node->first_node("component"); component_node; component_node = component_node->next_sibling())
			{
				if(strcmp(component_node->first_attribute("type")->value(),"transform") == 0)
				{
					for (xml_node<>* atributes_node = component_node->first_node("position"); atributes_node; atributes_node = atributes_node->next_sibling())
					{
						std::array<string, 3> ar;
						std::stringstream ss(atributes_node->value());
						while (ss.good())
						{
							for (int i = 0; i < ar.size(); i++)
							{
								string substr;
								getline(ss, substr, ',');
								ar[i] = substr;
							}
						};

						if(strcmp(atributes_node->name(), "position") == 0)
						{
							Vector3 new_pos{ std::stof(ar[0]),std::stof(ar[1]),std::stof(ar[2]) };
							t->position.equals(new_pos); 
						}

						else if(strcmp(atributes_node->name(), "rotation") == 0)
						{
							Vector3 new_rot{ std::stof(ar[0]),std::stof(ar[1]),std::stof(ar[2]) };
							t->rotation.equals(new_rot);
						}

						else
						{
							Vector3 new_scale{ std::stof(ar[0]),std::stof(ar[1]),std::stof(ar[2]) };
							t->scale.equals(new_scale);
						}
						e->set_original_transform(); 
					}
				}

				else if(strcmp(component_node->first_attribute("type")->value(), "render") == 0)
				{
					if (strcmp(component_node->first_attribute("class")->value(), "model") == 0)
					{
						xml_node<>* atribute_node = component_node->first_node("path");
						std::shared_ptr<Component> renderer(new Renderer_Component(atribute_node->value()));
						e->add_component("Renderer", renderer);
					}
					else
					{
						std::shared_ptr<Component> renderer(new Renderer_Component(RenderClass::Cube));
						e->add_component("Renderer", renderer);
					}
				}
			}
		}
	}
	void Scene::reset_scene()
	{
		map<string, shared_ptr<Entity>>::iterator it = Entity_Map.begin();
		while (it != Entity_Map.end())
		{
			it->second->reset_transform();
			it++;
		}
	}
	Entity& Scene::add_entity(float pos_x, float pos_y, float pos_z, float rot_x, float rot_y, float rot_z, float scale_x, float scale_y, float scale_z, string name)
	{
		Entity* e = new Entity(pos_x,   pos_y,   pos_z,
							   rot_x,   rot_y,   rot_z,
							   scale_x, scale_y, scale_z, name);
		Entity_Map.insert(std::make_pair(name, e));
		e->set_parent(*root);
		return *e;
	}
}
