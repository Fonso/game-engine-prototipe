/*
	Started by Alfonso L�pez Viejo on dicember of 2019
	Author: Alfonso L�pez Viejo
	Copyright � 2020 Alfonso L�pez Viejo
	Last Update: January/2020
	email: alfonsolopezviejo@gmail.com
*/

#include <Components.hpp>
#include <iostream>
#include <Entity.hpp>
#include <glm.hpp>
#include <Transform_Component.hpp>

using namespace glm; 
namespace engine 
{
	Transform_Component::Transform_Component()
	{
		position = { 0,0,0 }; 
		rotation = { 0,0,0 };
		scale    = { 0,0,0 };  
	}

	Transform_Component::Transform_Component(float   posX,   float posY,   float posZ, 
											 float   rotX,   float rotY,   float rotZ,
											 float scaleX, float scaleY, float scaleZ)
	{
		position = {posX, posY, posZ};
		rotation = {rotX, rotY, rotZ };
		scale    = { scaleX, scaleY, scaleZ }; 
	}

	bool Transform_Component::initialize()
	{
		return false;
	}

	bool Transform_Component::parse_property(const string& name, const string& value)
	{
		return false;
	}

	void Transform_Component::translate(float disX, float disY, float disZ)
	{
		position = { position.x + disX, position.y + disY, position.z + disZ };
	}
	

	float Transform_Component::magnitude(Vector3 point_a, Vector3 point_b)
	{

		glm::vec3 final_vector(point_b.x - point_a.x , point_b.y - point_a.y, point_b.z - point_a.z);
		return glm::length(final_vector); 
	}

	Vector3 Vector3::normalize()
	{
		glm::vec3 final_vector(this->x,this->y, this->z);
		glm::normalize(final_vector); 
		return Vector3{ final_vector.x, final_vector.y, final_vector.z};
	}

	float Vector3::magnitude()
	{
		glm::vec3 final_vector(this->x, this->y, this->z); 
		return glm::length(final_vector); 
	}
	void Vector3::equals(Vector3 const& new_vector)
	{
		this->x = new_vector.x; 
		this->y = new_vector.y; 
		this->z = new_vector.z; 
	}
}
