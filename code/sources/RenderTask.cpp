/*
	Started by Alfonso L�pez Viejo on dicember of 2019
	Author: Alfonso L�pez Viejo
	Copyright � 2020 Alfonso L�pez Viejo
	Last Update: January/2020
	email: alfonsolopezviejo@gmail.com
*/

#include <iostream>
#include <Scene.hpp>
#include <memory>
#include <map>
#include <Cube.hpp>
#include <Model.hpp>
#include <Model_Obj.hpp>
#include <Light.hpp>
#include <Render_Node.hpp>
#include <Window.hpp>
#include <Task.hpp>
#include <OpenGL.hpp>
#include <glm/gtx/quaternion.hpp>
#include <RenderTask.hpp>


using namespace std;
using namespace glt; 

namespace engine 
{

	RenderTask::RenderTask(int _priority, Window& w, Scene &scene) : Task{ priority }
	{
		m_window = &w;
		m_scene = &scene; 
		renderer.reset(new Render_Node);
			
		shared_ptr< Camera > camera(new Camera(90.f, 0.5f, 200.f, 1.f));
		shared_ptr< Light  > light(new Light);
		
		renderer->add("camera", camera);
		renderer->add("light", light);
		renderer->get("camera")->translate(glt::Vector3(0.f, 2.f, 2.f));
		renderer->get("camera")->rotate_around_x(-1.f); 
		renderer->get("light")->translate(glt::Vector3(10.f, 10.f, 10.f));
		check_renderers(); 
		set_transform(); 
	}

	RenderTask::~RenderTask()
	{
	}

	bool RenderTask::initialize()
	{
		//renderer = new Sample_Renderer(*m_window); 
		GLsizei width = GLsizei(m_window->get_width());
		GLsizei height = GLsizei(m_window->get_height());

		renderer->get_active_camera()->set_aspect_ratio(float(width) / height);

		glViewport(0, 0, width, height);
		return true;
	}

	bool RenderTask::finalize()
	{
		//renderer.reset(new Render_Node); 
		return true;
	}

	bool RenderTask::do_step(float time)
	{
		check_renderers();
		set_transform();
		m_window->clear();
		renderer->render();
		m_window->swap_buffers();
		return true;
	}

	void RenderTask::check_renderers()
	{
		std::map<std::string, std::shared_ptr<Entity>>::iterator it = m_scene->get_entity_map().begin();
		while (it != m_scene->get_entity_map().end())
		{
			if (!renderer->get(it->first))
			{
				if (it->second->find_component("Renderer"))
				{
					//cojo la ruta del objeto 
					Component* c = it->second->get_component("Renderer");
					if (Renderer_Component* renderer_comp = dynamic_cast<Renderer_Component*>(c))
					{
						switch (renderer_comp->get_class())
						{
							case RenderClass::Model:
								charge_model(renderer_comp, it->first);
								break; 
							case RenderClass::Cube: 
								shared_ptr<Model> c(new Model);
								c->add(shared_ptr< Drawable >(new Cube), Material::default_material());
								renderer->add(it->first, c);
								break;
						}	
					}
				}
			}
			it++;
		}
	}

	void RenderTask::set_transform()
	{
		std::map<std::string, std::shared_ptr<Entity>>::iterator it = m_scene->get_entity_map().begin();
		while (it != m_scene->get_entity_map().end())
		{
			if (it->second->find_component("Renderer"))
			{
				//cojo la ruta del objeto 
				Component* c = it->second->get_component("Renderer");
				if (Renderer_Component* renderer_comp = dynamic_cast<Renderer_Component*>(c))
				{
					if (renderer_comp)
					{
						auto model = renderer->get(it->first);
						Transform_Component *t = it->second->get_transform(); 

						Matrix44 pos_mat(1.f,			0.f,			0.f,			0.f,
										 0.f,			1.f,			0.f,			0.f,
										 0.f,			0.f,			1.f,			0.f,
										 t->position.x, t->position.y, t->position.z,	1.f);

						Matrix44 scale_mat(	t->scale.x, 0.f,		0.f,		0.f,
											0.f,		t->scale.y, 0.f,		0.f,
											0.f,        0.f,		t->scale.z,	0.f,
											0.f,		0.f,		0.f,		1.f);
						
						glm::vec3 eulerAngles(t->rotation.x, t->rotation.y, t->rotation.z); 
						glm::quat myQuaternion = glm::quat(eulerAngles); 
						Matrix44 rotationMat(glm::toMat4(myQuaternion)); 

						Matrix44 final_mat(pos_mat * scale_mat * rotationMat); 
						
						model->set_transformation(final_mat); 
	
					}
				}
			}
			it++;
		}
	}
	void RenderTask::charge_model(Renderer_Component* render_comp,const std::string &name) 
								 
	{
		shared_ptr<Model> model(new Model_Obj(render_comp->get_route()));
		renderer->add(name, model);
		std::cout << "Modelo cargado: " << name << " ";
	}
}
