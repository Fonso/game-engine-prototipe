/*
	Started by Alfonso L�pez Viejo on dicember of 2019
	Author: Alfonso L�pez Viejo
	Copyright � 2020 Alfonso L�pez Viejo
	Last Update: January/2020
	email: alfonsolopezviejo@gmail.com
*/

#pragma once
#include <Dispatcher.hpp>

namespace engine
{
	static Dispatcher *instance; 
	Dispatcher* Dispatcher::get_instance()
	{
		if (!instance)
		{
			instance = new Dispatcher();
		}
		return instance;	
	}

	void Dispatcher::register_listener(const string& message_id, Observer& listener)
	{
		listeners[message_id].push_back(&listener);	
	}

	void Dispatcher::unregister_listener(const string& message_id, Observer& listener)
	{
		listeners[message_id].remove(&listener);
	}

	void Dispatcher::multicast(const Message& message)
	{
		map< string, list<Observer*> >::iterator it = listeners.find(message.get_id());
		if (it != listeners.end())
		{
			for (auto listener : it->second)
			{
				listener->handle_message(message);
			}
		}	
	}
}