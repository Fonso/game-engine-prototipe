/*
	Started by Alfonso L�pez Viejo on dicember of 2019
	Author: Alfonso L�pez Viejo
	Copyright � 2020 Alfonso L�pez Viejo
	Last Update: January/2020
	email: alfonsolopezviejo@gmail.com
*/

#include <Components.hpp>
#include <Entity.hpp>
#include <Window.hpp>
#include <Scene.hpp>
#include <Message.hpp>
#include <Observer.hpp>
#include <string>
#include <Player.hpp>

using namespace engine;

void Player::handle_message(const Message& message)
{

	if(message.get_id() == "up" && transform->position.z >= -37.0f)
	{ 
		transform->translate(0,0,-_speed); 
	}

	else if (message.get_id() == "left" && transform->position.x >= -40.0f)
	{
		transform->translate(-_speed, 0, 0);
	}

	else if (message.get_id() == "right" && transform->position.x <= 41.0f)
	{
	
		transform->translate(_speed, 0, 0);
	}

	else if (message.get_id() == "down" && transform->position.z <= 0.f)
	{	
		transform->translate(0, 0, _speed);
	}

	else
	{
		sound->play(0,0); 
	}

}
