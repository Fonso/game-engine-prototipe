/*
	Started by Alfonso L�pez Viejo on dicember of 2019
	Author: Alfonso L�pez Viejo
	Copyright � 2020 Alfonso L�pez Viejo
	Last Update: January/2020
	email: alfonsolopezviejo@gmail.com
*/

#include "InputTask.hpp" 

bool engine::InputTask::initialize()
{
	return false;
}

bool engine::InputTask::finalize()
{

	return false;
}

bool engine::InputTask::do_step(float time)
{ 
	m_window->poll(*m_event, input_handler); 
	input_handler->deque_keys(); 
	return false;
}
