/*
	Started by Alfonso L�pez Viejo on dicember of 2019
	Author: Alfonso L�pez Viejo
	Copyright � 2020 Alfonso L�pez Viejo
	Last Update: January/2020
	email: alfonsolopezviejo@gmail.com
*/

#include "..\headers\InputMapper.hpp"
namespace engine
{
	std::string InputMapper::map_key(Keyboard::Key_Code key_code)
	{
		switch (key_code)
		{
		case Keyboard::KEY_A:	return "left";
		case Keyboard::KEY_S:   return "down";
		case Keyboard::KEY_D:	return "right";
		case Keyboard::KEY_W:	return "up";
		}
		return "unknown";
	}
	void InputMapper::send_message(Keyboard::Key_Code key_code)
	{
		Message m(map_key(key_code));
		Dispatcher::get_instance()->multicast(m);
	}
}