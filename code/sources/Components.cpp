/*
    Started by Alfonso L�pez Viejo on dicember of 2019
    Author: Alfonso L�pez Viejo
    Copyright � 2020 Alfonso L�pez Viejo
    Last Update: January/2020
    email: alfonsolopezviejo@gmail.com
*/

#include "Components.hpp"

namespace engine
{
	Component::Component(Entity* p)
	{
		parent = p; 
	}
}