/*
	Started by Alfonso L�pez Viejo on dicember of 2019
	Author: Alfonso L�pez Viejo
	Copyright � 2020 Alfonso L�pez Viejo
	Last Update: January/2020
	email: alfonsolopezviejo@gmail.com
*/


#include <SDL.h>
#include <Scene.hpp>
#include <UpdateTask.hpp>

namespace engine 
{
	UpdateTask::UpdateTask(int priority,Scene &s)
	{
		m_scene = &s;

	}

	bool engine::UpdateTask::initialize()
	{
		return false;
	}

	bool engine::UpdateTask::finalize()
	{

		return false;
	}

	bool engine::UpdateTask::do_step(float time)
	{
		std::map<std::string, std::shared_ptr<Entity>>::iterator it = m_scene->get_entity_map().begin();
		//recorro todo el mapa 
		while (it != m_scene->get_entity_map().end())
		{
			if(it->second->find_component("Script"))
			{
				//hago algo
				Component *c = it->second->get_component("Script"); 
				if (Script_Component* script = dynamic_cast<Script_Component*>(c)) 
				{
					if (script)
					{
						script->Update();
					}
				}
			}
			it++;
		}
		return true; 
	}

}
