/*
	Started by Alfonso L�pez Viejo on dicember of 2019
	Author: Alfonso L�pez Viejo
	Copyright � 2020 Alfonso L�pez Viejo
	Last Update: January/2020
	email: alfonsolopezviejo@gmail.com
*/

#include <Entity.hpp>

namespace engine 
{
	Entity::Entity(string name)
	{
		transform = new Transform_Component(0.f,  0.f,  0.f,
											0.f,  0.f,  0.f,
											1.0f, 1.0f, 1.0f);
		original_transform = *transform;
	}

	Entity::Entity(float posX, float posY, float posZ, float rotX, float rotY, float rotZ, float scaleX, float scaleY, float scaleZ, string name)
	{
		transform = new Transform_Component(posX,   posY,	posZ,
											rotX,   rotY,	rotZ,
											scaleX, scaleY, scaleZ);
		original_transform = *transform;
		initialize();
	}

	Entity::~Entity()
	{
		delete transform; 
		delete parent; 
	}

	bool Entity::initialize()
	{
		bool result = true;

		for (auto component : components)
		{
			if (component.second->initialize() == false)
			{
				result = false;
			}
		}
		return result;
	}

	bool Entity::add_component(const string& type, shared_ptr<Component>& component)
	{
		if (components.count(type) != 0)
		{
			return false;
		}
		else
		{
			components[type] = component;
			return true;
		}
	}

	bool Entity::find_component(std::string component_name)
	{
		map< string, shared_ptr< Component > >::iterator it = components.find(component_name);
		if (it != components.end())
		{
			return true;
		}
		return false;
	}

	Component* Entity::get_component(string type)
	{
		map< string, shared_ptr< Component > >::iterator it = components.find(type);
		if (it != components.end())
		{
			return it->second.get();
		}
		return nullptr;
	}
	Transform_Component* Entity::get_transform()
	{
		return transform;	
	}

	std::string Entity::get_name()
	{
		return name;		
	}

	void Entity::set_parent(Entity& new_parent)
	{
		parent = &new_parent;
	}

	void Entity::reset_transform()
	{
		*transform = original_transform;
	}



}
