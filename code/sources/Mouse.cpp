/*
	Started by Alfonso L�pez Viejo on dicember of 2019
	Author: Alfonso L�pez Viejo
	Copyright � 2020 Alfonso L�pez Viejo
	Last Update: January/2020
	email: alfonsolopezviejo@gmail.com
*/

#include <SDL.h>
#include <Mouse.hpp>

namespace engine
{
	Mouse::Mouse_Code Mouse::translate_sdl_mouse_code(int sdl_key_code)
	{
		switch(sdl_key_code)
		{
		case SDL_BUTTON_LEFT:	 return MOUSE_LEFT_CLICK; 
		case SDL_BUTTON_RIGHT:   return MOUSE_RIGHT_CLICK;
		case SDL_BUTTON_MIDDLE:  return MOUSE_MIDDLE_CLICK; 
		}
		return MOUSE_UNKOWN; 
	}
}
