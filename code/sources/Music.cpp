/*
	Started by Alfonso L�pez Viejo on dicember of 2019
	Author: Alfonso L�pez Viejo
	Copyright � 2020 Alfonso L�pez Viejo
	Last Update: January/2020
	email: alfonsolopezviejo@gmail.com
*/


#include <SDL_mixer.h>
#include <SDL.h>
#include <Music.hpp>

namespace engine 
{
	Music::Music(int frecuency, std::uint8_t channels, std::uint16_t samples)
	{
		_frecuency = frecuency;
		_channels = channels;
		_samples = samples;
		if (Mix_OpenAudio(_frecuency, MIX_DEFAULT_FORMAT, _channels, _samples) < 0)
		{
			SDL_Log("No se ha podido inicializar el subsistema de audio");
		}
	}

	bool Music::is_audio_init()
	{
		return SDL_INIT_AUDIO == NULL;
	}

	void Music::load_and_play(const char* path, int loops)
	{
		load_audio(path);
		play(loops);
	}

	void Music::change_volume(int new_vol)
	{
		Mix_VolumeMusic(new_vol);
	}

	void Music::FadeInMusic(int loops, int ms)
	{
		Mix_FadeInMusic(Music::music, loops, ms);
	}

	void Music::FadeOutMusic(int ms)
	{
		Mix_FadeOutMusic(ms);
	}

	void Music::load_audio(const char* path)
	{
		if (Music::music = Mix_LoadMUS(path))
		{
			SDL_Log("Cancion cargada!");
		}
		else
		{
			SDL_Log(Mix_GetError());
		}
	}

	void Music::play(int loops)
	{
		if (Music::music)
		{
			Mix_PlayMusic(Music::music, loops);
		}

		else
		{
			SDL_Log("No hay ninguna cancion cargada");
		}
	}

	void Music::pause()
	{
		if (Mix_PlayingMusic != 0)
		{
			Mix_PauseMusic();
		}
	}

	void Music::resume()
	{
		if (Mix_PausedMusic != 0)
		{
			Mix_ResumeMusic();
		}
	}

}