/*
	Started by Alfonso L�pez Viejo on dicember of 2019
	Author: Alfonso L�pez Viejo
	Copyright � 2020 Alfonso L�pez Viejo
	Last Update: January/2020
	email: alfonsolopezviejo@gmail.com
*/

#include <Kernel.hpp>

namespace engine
{
	void Kernel::execute()
	{
		exit = false;

		for (auto task : tasks)
		{
			task->initialize();
		}

		float time = 1.f / 60.f;

		while (!exit)
		{
			Timer timer;

			for (auto task : tasks)
			{
				if (exit)
				{
					break;
				}
				task->do_step(time);
			}

			time += timer.elapsed_seconds();

		}

		for (auto task : tasks)
		{
			task->finalize();
		}
	}
}