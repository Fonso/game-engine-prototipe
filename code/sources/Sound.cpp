/*
	Started by Alfonso L�pez Viejo on dicember of 2019
	Author: Alfonso L�pez Viejo
	Copyright � 2020 Alfonso L�pez Viejo
	Last Update: January/2020
	email: alfonsolopezviejo@gmail.com
*/

#include <SDL_mixer.h>
#include <SDL.h>
#include <Sound.hpp>

namespace engine 
{
	Sound::Sound(int frecuency, std::uint8_t channels, std::uint16_t samples)
	{
		_frecuency = frecuency;
		_channels = channels;
		_samples = samples;
		if (Mix_OpenAudio(_frecuency, MIX_DEFAULT_FORMAT, _channels, _samples) < 0)
		{
			SDL_Log("No se ha podido inicializar el subsistema de audio");
		}
	}

	void Sound::load_and_play(const char* path, int loops, int channel)
	{
		_channels = channel;
		load_audio(path);
		play(loops, channel);
	}

	void Sound::Change_Volume_Chunk(int volume)
	{
		Mix_VolumeChunk(Sound::sound, volume);
	}

	void Sound::FadeInSound(int channel, int loops, int ms, int ticks)
	{
		Mix_FadeInChannelTimed(channel, Sound::sound, loops, ms, ticks);
	}

	void Sound::FadeOutSound(int ms)
	{
		Mix_FadeOutChannel(0, ms);
	}

	void Sound::play(int loops, int channel)
	{
		_channels = channel;
		if (Sound::sound)
		{
			Mix_PlayChannel(_channels, Sound::sound, loops);
		}
		else
		{
			SDL_Log("No hay ningun sonido cargado");
		}
	}


	void Sound::pause()
	{
		if (Mix_PlayingMusic != 0)
		{
			Mix_PauseMusic();
		}
	}

	void Sound::resume()
	{
		if (Mix_PausedMusic != 0)
		{
			Mix_ResumeMusic();
		}
	}

	void Sound::load_audio(const char* path)
	{
		if (sound = Mix_LoadWAV(path))
		{
			SDL_Log("Sonido cargado!");
		}
		else
		{
			SDL_Log(path, Mix_GetError());
		}
	}
}