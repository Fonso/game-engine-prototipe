/*
    Started by Alfonso L�pez Viejo on dicember of 2019
    Author: Alfonso L�pez Viejo
    Copyright � 2020 Alfonso L�pez Viejo
    Last Update: January/2020
    email: alfonsolopezviejo@gmail.com
*/

#pragma once
#include <Message.hpp>
#include <list>

namespace engine 
{
    /**
    *   @brief Observer class
    *   @details Recibe objetos de la clase Message y realiza una accion cuando los recibe
    */
    class Observer
    {
    public:

        /**
        *   @brief handle_message
        *   @param message mensaje que estamos escuchando
        *   @details Define la accion que se realiza cuando se recibe un mensaje
        */
        virtual void handle_message(const Message& message) = 0;

    };
}