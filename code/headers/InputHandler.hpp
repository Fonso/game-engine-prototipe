/*
	Started by Alfonso L�pez Viejo on dicember of 2019
	Author: Alfonso L�pez Viejo
	Copyright � 2020 Alfonso L�pez Viejo
	Last Update: January/2020
	email: alfonsolopezviejo@gmail.com
*/

#pragma once
#include <string>
#include <Keyboard.hpp>
#include <InputMapper.hpp>
#include <queue>

namespace engine 
{
	/**
	* @brief InputHandler Class
	* @details La clase guarda todas las teclas pulsadas por el usuario que despues 
	* pasa a la clase InputMapper para que las convierta en mensajes
	*/
	class InputHandler
	{
	private: 
		/**
		*	@brief keys_pressed
		*	@details Cola de teclas pulsadas por el usuario
		*/
		std::queue<Keyboard::Key_Code> keys_pressed; 
		/**
		*	@brief mapper
		*	@details Referencia al InputMapper
		*/
		InputMapper* mapper = nullptr; 

	public: 
		/**
		*	@brief enque_key
		*	@params key codigo de la tecla pulsada 
		*	@details encola una nueva tecla pulsada por el usuario
		*/
		void enque_key(Keyboard::Key_Code key)
		{
			keys_pressed.push(key);
		}
		/**
		*	@brief enque_key
		*	@details encola una nueva tecla pulsada por el usuario
		*/
		void deque_keys()
		{
			while (!keys_pressed.empty())
			{
				mapper->send_message(keys_pressed.front()); 
				keys_pressed.pop(); 
			}
		}
	}; 
}
