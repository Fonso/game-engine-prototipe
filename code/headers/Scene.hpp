/*
	Started by Alfonso L�pez Viejo on dicember of 2019
	Author: Alfonso L�pez Viejo
	Copyright � 2020 Alfonso L�pez Viejo
	Last Update: January/2020
	email: alfonsolopezviejo@gmail.com
*/

#pragma once
#include <Kernel.hpp>
#include <map>
#include <string>
#include <memory>
#include <Entity.hpp>
#include <InputTask.hpp>
#include <RenderTask.hpp>
#include <Renderer_Component.hpp>
#include <Script_Component.hpp>
#include <fstream>
#include <UpdateTask.hpp>
#include <Components.hpp>

using namespace std; 
 
namespace engine
{
	/**
	* @brief Scene Class
	* @details Permite crear nuevas escenas con entidades, tareas y una ventana
	*/
	class Scene
	{
		/**
		*	@brief mapa de entidades
		*/
		std::map<std::string, std::shared_ptr<Entity> > Entity_Map; 
		Kernel     *kernel;
		InputTask  *input_task; 
		RenderTask *render_task; 
		UpdateTask *update_task; 
		Entity     *root;
		Window	   *m_window; 

	public:

		/**
			@brief load
			@details Carga la escena descrita en un xml
		*/
		Scene(const string& scene_description_file_path, Window& w); 

		std::map<std::string, std::shared_ptr<Entity>> &get_entity_map()
		{
			return Entity_Map; 
		}

	private:
		/**
			@brief load
			@param scene_description_file_path Ruta del XML
			@details Carga la escena descrita en un xml
		*/
		void load(const string& scene_description_file_path); 

		/**
			@brief init_kernel
			@details Inicializa el kernel
		*/
		void init_kernel()
		{	
			kernel = new Kernel(); 
			render_task = new RenderTask(0, *m_window, *this);
			update_task = new UpdateTask(2, *this);
			input_task =  new InputTask(1, m_window);
			kernel->add_task(*input_task);
			kernel->add_task(*render_task);
			kernel->add_task(*update_task); 
		}
	public: 

		/**
			@brief get_window
			@details Devuelve la ventana de la escena
		*/
		Window &get_window()const 
		{
			return *m_window; 
		}

		/**
			@brief reset_scene
			@details Devuelve a todas las entidades a su posicion de inicio
		*/
		void reset_scene(); 


		/**
			@brief execute_scene
			@details Ejecuta el kernel
		*/
		void execute_scene()
		{
			kernel->execute();
		}


		/**
			@brief add_entity
		*	@param posX Determina la posici�n en X de la entidad
		*	@param posY Determina la posici�n en Y de la entidad
		*	@param posZ Determina la posici�n en Z de la entidad
		*	@param rotX Determina la rotaci�n en X de la entidad
		*	@param rotY Determina la rotaci�n en Y de la entidad
		*	@param rotZ Determina la rotaci�n en X de la entidad
		*	@param scaleX Determina la escala en X de la entidad
		*	@param scaleY Determina la escala en Y de la entidad
		*	@param scaleZ Determina la escala en Z de la entidad
		*   @param name Determina el nombre de la entidad
			@details A�ade una entidad a la escena
		*/
		Entity& add_entity(float pos_x,   float pos_y,   float pos_z,
						   float rot_x,   float rot_y,   float rot_z,
						   float scale_x, float scale_y, float scale_z, string name); 

	};


}
