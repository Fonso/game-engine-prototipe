/*
	Started by Alfonso L�pez Viejo on dicember of 2019
	Author: Alfonso L�pez Viejo
	Copyright � 2020 Alfonso L�pez Viejo
	Last Update: January/2020
	email: alfonsolopezviejo@gmail.com
*/

#pragma once
#include<Window.hpp>
#include<Task.hpp>

namespace engine
{
	class Scene; 
	/**
	* @brief Entity Class
	* @details Esta clase es un tipo de tarea que recorrer todas las entidades y ejecuta los codigos
	* de aquellas con un componente de tipo Script_Component
	*/
	class UpdateTask:public Task
	{
	private: 
		Scene *m_scene = nullptr; 

	public:
		/**
		* @brief UpdateTask
		* @details Constructor de la clase UpdateTask
		*/
		UpdateTask(int priority, Scene& s); 

		/** Heredado v�a Task */ 
		virtual bool initialize() override;

		virtual bool finalize() override;

		virtual bool do_step(float time) override;
	};
}