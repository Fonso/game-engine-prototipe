/*
	Started by Alfonso L�pez Viejo on dicember of 2019
	Author: Alfonso L�pez Viejo
	Copyright � 2020 Alfonso L�pez Viejo
	Last Update: January/2020
	email: alfonsolopezviejo@gmail.com
*/

#pragma once
#include <Window.hpp>
#include <Task.hpp>
#include <InputHandler.hpp>

namespace engine 
{
	/**
	* @brief InputTask 
	* @details La clase InputTask hereda de Task y se encarga de registrar y resolver  
	* las entradas que introduce el usuario
	*/
	class InputTask:public Task
	{
	private:
		/**
			@brief puntero a un objeto de la clase Input Handler
		*/
		InputHandler* input_handler = nullptr; 
		/**
			@brief Puntero a la ventana 
		*/
		Window* m_window = nullptr; 
		/**
			@brief Puntero al evento
		*/
		Window::Event *m_event = nullptr; 
	public:
		/**
			@brief InputTask
			@param _priority prioridad de la tarea
			@param w puntero a la ventana
			@details Constructor de la clase InputTask
		*/
		InputTask(int _priority, Window *w):Task(priority),m_window(w)
		{
			input_handler = new InputHandler(); 
			m_event = w->get_event(); 
		}
		/**
			@brief InputTask
			@param _priority prioridad de la tarea
			@param w puntero a la ventana
			@details Constructor de la clase InputTask
		*/
		~InputTask()
		{
			delete m_event; 
			delete m_window; 
			delete input_handler; 
		}
		// Heredado v�a Task
		/**
			@brief initialize
			@details Inicializa la entidad
		*/
		virtual bool initialize() override;
		/**
			@brief finalize
			@details Se ejecuta cuando se sale del loop principal del Kernel
		*/
		virtual bool finalize() override;

		/**
			@brief do_step
			@details Funcion que se ejecuta todo el rato en el kernel. Se encarga de 
			recibir las entradas del usuario y despues transformarlas en mensajes con la ayuda
			de las clases InputMapper e InputHandler
		*/
		virtual bool do_step(float time) override;

	};
}