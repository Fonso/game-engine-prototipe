/*
	Started by Alfonso L�pez Viejo on dicember of 2019
	Author: Alfonso L�pez Viejo
	Copyright � 2020 Alfonso L�pez Viejo
	Last Update: January/2020
	email: alfonsolopezviejo@gmail.com
*/

#pragma once
#include <Task.hpp>
#include <Components.hpp>

namespace engine
{
	/**
	* @brief Script_Component Class
	* @details Es un tipo de componente que se encarga de realizar los comportamientos 
	* de las entidades
	*/
	class Script_Component : public Component
	{
	public: 
		/**
		* @brief Script_Component
		* @param parent Entidad padre de la entidad
		* @details Constructor de script_component
		*/
		Script_Component(Entity* parent) : Component(parent)
		{
		}; 
		/**
		* @brief Update
		* @details Ejecuta en bucle los comportamientos dentro de la funcion
		*/
		virtual void Update(); 
	};
}