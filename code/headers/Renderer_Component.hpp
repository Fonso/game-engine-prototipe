/*
	Started by Alfonso L�pez Viejo on dicember of 2019
	Author: Alfonso L�pez Viejo
	Copyright � 2020 Alfonso L�pez Viejo
	Last Update: January/2020
	email: alfonsolopezviejo@gmail.com
*/

#pragma once
#include<Components.hpp>
#include<string>

namespace engine
{
	/**
	*   @brief Renderer_Class
	*   @details Define que tipo de objeto va a renderizar el componente
	*/
	enum class RenderClass
	{
		Model,Cube 
	};
	/**
	*   @brief Renderer_Component Class
	*   @details Tipo de componente que permite renderizar objetos en la escena
	*/
	class Renderer_Component:public Component
	{

	private: 
		/*
		* brief ruta donde se guarda el modelo del objeto que se quiere renderizar
		*/
		string obj_route; 
		RenderClass obj_class; 
	public: 
		/**
		*	@brief Renderer_Component
		*	@param route Ruta del modelo que queremos renderizar
		*	@details Constructor de la clase Renderer_Component cuando recibimos 
		*	la ruta de un modelo
		*/
		Renderer_Component(std::string route)
		{
			obj_route = route;
			obj_class = RenderClass::Model; 
		}
		/**
		*	@brief Renderer_Component
		*	@param route Ruta del modelo que queremos renderizar
		*	@details Constructor de la clase Renderer_Component cuando recibimos 
		*	la clase del objeto
		*/
		Renderer_Component(RenderClass render_class)
		{
			obj_class = render_class; 
		}

		/**
		*	@brief get_route
		*	@details Devuelve la ruta del objeto 
		*/
		const std::string get_route() const { return obj_route;  };
		/**
		*	@brief get_class
		*	@details Devuelve la clase de renderer
		*/
		const RenderClass get_class() const { return obj_class; };
		// Heredado v�a Component
		/**
		*	@brief initialize
		*	@details Inicializa el componente
		*/
		virtual bool initialize() override;
		virtual bool parse_property(const string& name, const string& value) override;
	};
}
