/*
	Started by Alfonso L�pez Viejo on dicember of 2019
	Author: Alfonso L�pez Viejo
	Copyright � 2020 Alfonso L�pez Viejo
	Last Update: January/2020
	email: alfonsolopezviejo@gmail.com
*/

#pragma once
#include <string>
#include <map>
#include <memory>
#include <Components.hpp>
#include <Transform_Component.hpp>

using namespace std; 
namespace engine 
{
	/**
	* @brief Entity Class
	* @details La clase entidad nos permite crear entidades en la escena	
	*/
	class Entity
	{
	private:
		/**
		* @brief puntero al componente transform 
		*/
		Transform_Component *transform = nullptr;
		/**
		* @brief copia de los valores de comienzo del componente transform
		*/
		Transform_Component original_transform; 
		/**
		* @brief nombre de la entidad 
		*/
		std::string name; 
		/**
		* @brief padre de la entidad
		*/
		Entity* parent = nullptr; 
		/**
		* @brief mapa de componentes de la entidad
		*/
		map< string, shared_ptr< Component > > components;

	public:
		/** 
		*	@brief Entity 
		*	@param name Determina el nombre de la entidad	
		*	@details Constructor por defecto de Entity 
		*/
		Entity(string name); 

		/**
		*	@brief Entity
		*	@param posX Determina la posici�n en X de la entidad
		*	@param posY Determina la posici�n en Y de la entidad
		*	@param posZ Determina la posici�n en Z de la entidad
		*	@param rotX Determina la rotaci�n en X de la entidad
		*	@param rotY Determina la rotaci�n en Y de la entidad
		*	@param rotZ Determina la rotaci�n en X de la entidad
		*	@param scaleX Determina la escala en X de la entidad
		*	@param scaleY Determina la escala en Y de la entidad
		*	@param scaleZ Determina la escala en Z de la entidad
		*   @param name Determina el nombre de la entidad
		*	@details Constructor de Entity con el que podemos determinar 
		*	su posicion, rotacion, escala y nombre
		*/

		Entity(float posX, float posY, float posZ,
			   float rotX, float rotY, float rotZ,
			   float scaleX, float scaleY, float scaleZ, string name); 
		/**
		*	@brief ~Entity
		*	@details Destructor de la clase Entity
		*/
		~Entity(); 
		 
		/**
		*	@brief initialize 
		*	@details inicializa todos los componentes de la entidad
		*/
		bool initialize(); 
		
		/**
		*	@brief add_component
		*	@param type tipo del componente que queremos a�adir 
		*	@component referencia del componente que queremos a�adir 
		*	@details Permite a�adir un componente a la lista de componentes 
		*	de la entidad. Si esta repetido, es decir si la entidad ya tiene otro 
		*	componente del mismo tipo, no lo a�ade
		*/
		bool add_component
		(
			const string& type,
			shared_ptr< Component >& component
		); 

		/**
		*	@brief find_component
		*	@param component_name nombre del tipo de componente que queremos buscar
		*	@details Busca si la entidad tiene un componente del tipo que se 
		*	le pregunte, en este caso del tipo que diga component_name
		*/
		bool find_component(std::string component_name); 

		/**
		*	@brief get_component
		*	@param type nombre del tipo de componente que queremos buscar
		*	@details Busca un componente dentro del mapa de entidades y devuelve 
		*	un puntero al mismo
		*/
		Component* get_component(string type); 

		/**
		*	@brief get_transform 
		*	@details Devuelve un puntero al componente transform de la entidad 
		*/

		Transform_Component* get_transform(); 

		/**
		*	@brief get_name
		*	@details Devuelve el nombre de la entidad
		*/
		std::string get_name(); 

		/**
		*	@brief set_parent
		*	@details Permite determinar el padre de la entidad
		*/
		void set_parent(Entity& new_parent); 

		/**
		*	@brief reset_transform
		*	@details Resetea los valores del componente transform a los valores con 
		*	los que empezo
		*/
		void reset_transform(); 


		/**
		*	@brief set_original_transform
		*	@details Resetea los valores del componente transform a los valores con
		*	los que empezo
		*/
		void set_original_transform()
		{
			original_transform = *transform; 
		}

	};
}