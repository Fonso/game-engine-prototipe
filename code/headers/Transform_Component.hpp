/*
	Started by Alfonso L�pez Viejo on dicember of 2019
	Author: Alfonso L�pez Viejo
	Copyright � 2020 Alfonso L�pez Viejo
	Last Update: January/2020
	email: alfonsolopezviejo@gmail.com
*/

#pragma once
#include <memory>
#include <internal/declarations.hpp>


namespace engine
{
	/**
	*   @brief Vector3 struct
	*   @details Esta estructura intenta emular a la del vector matem�tico de 3 componentes, para 
	*   poder realizar diversas operaciones con ello
	*/
	struct Vector3
	{
		float x, y, z;
		public: 
		/**
		*	@brief normalize 
		*	@details Devuelve el vector normalizado
		*/
		Vector3 normalize(); 
		/**
		*	@brief magnitude
		*	@details Devuelve la magnitud del vector
		*/
		float magnitude();
		/**
		*	@brief equals
		*	@param new_vector Vector al que se quiere igualar 
		*	@details Iguala los valores de este vector a otro dado
		*/
		void equals(Vector3 const &new_vector); 
		Vector3 operator * (float const &other)
		{
			return Vector3{this->x * other, this->y * other, this->z * other};
		}

		Vector3 operator + (Vector3 const &other)
		{
			return Vector3{ this->x + other.x, this->y + other.y, this->z + other.z };
		}

	};

	/**
	*   @brief Transform_Component
	*   @details Tipo de componente que define la posicion rotacion y escala de la entidad 
	*/
	class Transform_Component:public Component
	{
	public: 
		Vector3 position; 
		Vector3 rotation; 
		Vector3 scale; 
	public:
		/**
		*   @brief Transform_Component 
		*   @details Constructor de la clase Transform_Component
		*/
		Transform_Component(); 
		/**
		*   @brief Transform_Component
		*	@param posX Determina la posici�n en X de la entidad
		*	@param posY Determina la posici�n en Y de la entidad
		*	@param posZ Determina la posici�n en Z de la entidad
		*	@param rotX Determina la rotaci�n en X de la entidad
		*	@param rotY Determina la rotaci�n en Y de la entidad
		*	@param rotZ Determina la rotaci�n en X de la entidad
		*	@param scaleX Determina la escala en X de la entidad
		*	@param scaleY Determina la escala en Y de la entidad
		*	@param scaleZ Determina la escala en Z de la entidad
		*   @details Constructor de la clase Transform_Component dados una posicion, rotacion 
		*	y escala
		*/
		Transform_Component(float posX, float posY, float posZ,
							float rotX, float rotY, float rotZ,
							float scaleX, float scaleY, float scaleZ); 
	
		/**
		*	@brief translate 
		*	@param disX Desplazamiento en X
		*	@param disY Desplazamiento en Y 
		*	@param disZ Desplazamiento en Z
		*	@details Desplaza a la entidad una determinada distancia en X,Y y Z
		*/
		void translate(float disX, float disY, float disZ); 

		/**
		*	@brief magnitude
		*	@param point_a Punto a
		*	@param point_b Punto b
		*	@details Devuelve la magnitud entre 2 puntos
		*/
		float magnitude(Vector3 point_a, Vector3 point_b); 

		// Heredado v�a Component
		virtual bool initialize() override;

		virtual bool parse_property(const string& name, const string& value) override;

	};
}