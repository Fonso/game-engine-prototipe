/*
	Started by Alfonso L�pez Viejo on dicember of 2019
	Author: Alfonso L�pez Viejo
	Copyright � 2020 Alfonso L�pez Viejo
	Last Update: January/2020
	email: alfonsolopezviejo@gmail.com
*/

#pragma once

#include <memory>
#include <string>
#include <internal/declarations.hpp>
 
namespace engine
{
	class Window;
	class Scene; 
	class Renderer_Component; 
	/**
	*	@brief RenderTask Class
	*	@details La clase es un tipo de tarea que se encarga de renderizar todas las 
	*	entidades de la escena que tenga un componente de tipo renderer
	*/
	class RenderTask :public Task
	{
	private:
		/**
		*	@brief Puntero a la escena
		*/
		Scene  *m_scene  = nullptr; 
		/**
		*	@brief Puntero al Render Node
		*/
		std::unique_ptr< glt::Render_Node > renderer;
		/**
		*	@brief Puntero a la ventana
		*/
		Window* m_window = nullptr;

	private:
		/**
		*	@brief check_renderers 
		*	@details Recorre todas las entidades y si alguna tiene un componente renderer 
		*	la renderiza
		*/
		void check_renderers();
		/**
		*	@brief set_transforms 
		*	@details Comprueba si el transform component de una entidad con un 
		*	renderer_component ha cambiado sus valores y en caso afirmativo
		*	lo modifica en la escena.
		*/
		void set_transform();
		/**
		*	@brief charge_model
		*	@param render_comp Renderer_Component que quiere cargar el modelo
		*	@param name Nombre del modelo
		*	@details Carga un modelo en la escena
		*	lo modifica en la escena.
		*/
		void charge_model(Renderer_Component *render_comp, const string &name);

	public:
		RenderTask(int _priority, Window& w, Scene &scene); 
		~RenderTask(); 
		// Heredado v�a Task
		virtual bool initialize() override;
		virtual bool finalize() override;
		virtual bool do_step(float time) override;


		
	};
}