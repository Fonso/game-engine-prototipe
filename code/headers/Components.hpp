/*
	Started by Alfonso L�pez Viejo on dicember of 2019
	Author: Alfonso L�pez Viejo
	Copyright � 2020 Alfonso L�pez Viejo
	Last Update: January/2020
	email: alfonsolopezviejo@gmail.com
*/


#pragma once
#include <string>

using namespace std; 

namespace engine 
{
	class Entity;
	/**
	* @brief Component Class 
	* @details La clase Component permite crear componentes que podremos a�adir a entidades 
	*/
	class Component
	{
	protected:

		Entity* parent; //entidad en la que esta situado el componente

	public:
		/**
		* @brief Component
		* @details Constructor por defecto de components
		*/
		Component() { parent = nullptr; }; 
		/**
		* @brief Component 
		* @param p Entidad a la que pertenece el componente
		*/
		Component(Entity* p); 
		
		/**
		* @brief ~Component 
		* @details Destructor de la clase Component
		*/
		virtual ~Component()
		{
			delete parent; 
		}

		/**
		* @brief initialize 
		* @details Permite inicializar un componente
		*/
		virtual bool initialize() { return true;  };

		virtual bool parse_property
		(
			const string& name,
			const string& value
		) { return true; };

		/**
		* @brief get_entity
		* @details Devuelve la entidad a la que pertenece el componente
		*/
		Entity &get_entity()
		{
			return *parent; 
		}

	};
}
