/*
	Started by Alfonso L�pez Viejo on dicember of 2019
	Author: Alfonso L�pez Viejo
	Copyright � 2020 Alfonso L�pez Viejo
	Last Update: January/2020
	email: alfonsolopezviejo@gmail.com
*/

#pragma once

#include <cstdint>

namespace engine 
{
	/**
	*   @brief Timer class
	*   @details Esta clase crea cronometros que gestionan el tiempo que pasa
	*	en el motor
	*/
	class Timer
	{
		/*
		*	@brief Tiempo de comienzo
		*/
		uint32_t start_ticks;
		/*
		*	@brief Tiempo actual
		*/
		uint32_t actual_ticks; 

	public:

		/**
		*	@brief Timer
		*	@details Constructor de la clase Timer
		*/
		Timer()
		{
			start();
		}
		/**
		*	@brief Start
		*	@details Inicializa el tiempo de comienzo y el actual usando 
		*   el tiempo de SDL como referencia
		*/
		void start();
		/**
		*	@brief elapsed_seconds
		*	@details Tiempo que ha pasado entre fotogramas en segundos 
		*/
		float elapsed_seconds()const; 
		/**
		*	@brief Start
		*	@details Tiempo que ha pasado entre fotogramas en milisegundos
		*/
		uint32_t elapsed_milliseconds() const; 
	
	};
}