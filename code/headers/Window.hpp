
/*
    Started by Alfonso López Viejo on dicember of 2019
    Author: Alfonso López Viejo
    Copyright © 2020 Alfonso López Viejo
    Last Update: January/2020
    email: alfonsolopezviejo@gmail.com
*/

#pragma once

#include <string>
#include <Keyboard.hpp>
#include <internal/declarations.hpp>

namespace engine
{
    class InputHandler; 
    /**
    * @brief Window Class
    * @details Esta clase permite crear ventanas gracias a SDL2
    */
    class Window
    {
    public:

        /** 
        *   @brief Event Struct
        *   @details Esta clase Event es específica para los eventos de la ventana.
        *   Por ello está anidada dentro de Window. Se podría generalizar.
        */
        struct Event
        {

            Event() 
            {
                type = Type::KEY_RELEASED; 
                data.keyboard.key_code = 0; 
                data.mouse.x = 0;
                data.mouse.y = 0;
                data.mouse.buttons = 0;
            };
            enum Type
            {
                CLOSE,
                KEY_PRESSED,
                KEY_RELEASED,
                MOUSE_DOWN, 
                MOUSE_MOTION, 
                MOUSE_UP
            }
            type;

            union Data
            {
                struct { int key_code; } keyboard;

                struct
                {
                    int x, y;
                    int   buttons;
                }
                mouse;
            }
            data;
        };

    private:

        SDL_Window  * window;
        SDL_GLContext gl_context;

    public:
        /**
        *	@brief Window
        *	@param title Titulo de la ventana
        *   @param width Ancho de la ventana
        *   @param height Alto de la ventana
        *   @param fullscreen Determina si esta en pantalla completa o no 
        *	@details Constructor de la clase Window
        */
        Window(const std::string & title, int width, int height, bool fullscreen = false);
        /**
        *   @brief ~Window 
        *   @details Destructor de la clase Window
        */
        ~Window();

    public:
		/**
        *   @brief get_event
        *   @details Retorna el evento asociado a la ventana 
        */
		Event* get_event();
        /**
        *   @brief get_width
        *   @details Retorna el ancho actual de la ventana (el usuario puede cambiarlo).
        */
        unsigned get_width () const;

        /** 
        *   @brief get_height 
        *   @details Retorna el alto actual de la ventana (el usuario puede cambiarlo).
        */
        unsigned get_height () const;

        /** 
        *   @brief poll
        *   @param event Evento a extraer 
        *   @param input_handler Puntero al input_handler
        *   @details Permite extraer un evento de la cola de eventos asociada a la ventana.
        */
        bool poll (Event & event, InputHandler *input_handler) const;

        /**
        *   @brief enable_vsync 
        *   @details Activa la sincronizacion vertical
        */
        void enable_vsync ();

        /**
        *   @brief disable_vsync
        *   @details Desactiva la sincronizacion vertical
        */
        void disable_vsync ();

        /** 
        *   @brief clear 
        *   @details Borra el buffer de la pantalla usando OpenGL.
        */
        void clear () const;

        /** 
        *   @brief swap_buffers 
        *   @details Intercambia el buffer visible con el buffer oculto.
        */
        void swap_buffers () const;

        /** 
        *   @brief set_window_fullscreen 
        *   @details Convierte la ventana a pantalla completa 
        */
        void set_window_fullscreen()const; 

        /** 
        *   @brief set_window_resizable
        *   @details Permite redimensionar la ventana
        */
        void set_window_resizable()const; 

        /**
        *   @brief set_window_bordered 
        *   @details Permite cambiar la ventana al modo con bordes 
        */
        void set_window_bordered()const; 

        /** 
        *   @brief hide_window
        *   @details Permite ocultar la ventana 
        */ 
        void hide_window()const; 

        /**
        *   @brief show_window 
        *   @details Permite volver a ver la ventana 
        */
        void show_window()const;

        /**
        *   @brief maximize_window
        *   @details Permite maximizar la ventana 
        */
        void maximize_window()const;

        /** 
        *   @brief minimice_window
        *   @details Permite minimizar la ventana 
        */
        void minimice_window()const;

        /** 
        *    @brief restore_window 
        *    @details Resetea la posicion y la dimension de la ventana 
        */
        void restore_window()const;

        /**
        *   @brief destroy_window 
        *   @details Destruye la ventana 
        */
        void destroy_window()const;

        /** 
        *    @brief set_window_position
        *    @details Permite modificar la posicion de la ventana 
        */
        void set_window_position(int new_x, int new_y);

        /** 
        *   @brief set_window_centered
        *    @details Centra la ventana 
        */
        void set_window_centered(); 

        /**
        *   @brief set_window_title   
        *   @param new_title Nuevo titulo de la ventana
        *   @details Cambia el titulo de la ventana 
        */
        void set_window_title(const char* new_title); 
        
    };

}
