/*
	Started by Alfonso L�pez Viejo on dicember of 2019
	Author: Alfonso L�pez Viejo
	Copyright � 2020 Alfonso L�pez Viejo
	Last Update: January/2020
	email: alfonsolopezviejo@gmail.com
*/

#pragma once
 
namespace engine
{
	/**
	*   @brief Music class
	*   @details Esta clase se encarga de crear tareas que seran reproducidas 
	*	por un kernel
	*/
	class Task
	{
	
	protected:  
		int priority; 
		bool doTask = true; 

	public:

		Task() = default; 
		/**
		*	@brief Task
		*	@param priority Prioridad de la tarea
		*	@details Constructor de la clase Task
		*/
		Task(int priority) :priority(priority)
		{
		}
	public:
		/**
		*	@brief initialize 
		*	@details Inicializa la tarea
		*/
		virtual bool initialize() = 0;
		/**
		*	@brief finalize 
		*	@details Finaliza la tarea
		*/
		virtual bool finalize() = 0;
		/**
		*	@brief do_step
		*	@details Ejecuta esta funcion cada pasada del bucle principal 
		*	del kernel
		*/
		virtual bool do_step(float time) = 0;

	public:

		bool operator < (const Task& other) const
		{
			return this->priority < other.priority;
		}

		bool operator > (const Task& other) const
		{
			return this->priority > other.priority;
		}

		bool operator = (const Task& other) const
		{
			return this->priority == other.priority; 
		}
	};

}