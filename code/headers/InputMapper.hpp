/*
	Started by Alfonso L�pez Viejo on dicember of 2019
	Author: Alfonso L�pez Viejo
	Copyright � 2020 Alfonso L�pez Viejo
	Last Update: January/2020
	email: alfonsolopezviejo@gmail.com
*/

#pragma once
#include <Keyboard.hpp>
#include <Message.hpp>
#include <Dispatcher.hpp>
#include <string> 

namespace engine
{
	/**
	* @brief InputMapper Class
	* @details La clase transforma las entradas del usuario en mensajes que 
	* pueden ser escuchados por observers para su utilizacion
	*/
	class InputMapper
	{
	public: 
		/**
		*	@brief map_key 
		*	@params key_code Codigo de la tecla a transformar
		*	@details Transforma una tecla en mensaje
		*/
		std::string map_key(Keyboard::Key_Code key_code); 

		/**
		*	@brief send_message
		*	@params key_code Codigo de la tecla que queremos mandar un mensaje
		*	@details Transforma la tecla introducida por el usuario en mensaje 
		*	y lo manda
		*/
		void send_message(Keyboard::Key_Code key_code); 

	};
}