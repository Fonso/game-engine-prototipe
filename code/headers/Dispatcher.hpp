/*
	Started by Alfonso L�pez Viejo on dicember of 2019
	Author: Alfonso L�pez Viejo
	Copyright � 2020 Alfonso L�pez Viejo
	Last Update: January/2020
	email: alfonsolopezviejo@gmail.com
*/

#pragma once
#include <Message.hpp>
#include <Observer.hpp>

namespace engine
{
	/**
	* @brief Dispatcher Class 
	* @details Esta clase se encarga de enviar mensajes a los Observers que esten escuchando el mensaje
	*/
	class Dispatcher
	{
	private: 
		/**
		* @brief Dispatcher
		* @details Constructor por defecto de la clase Dispatcher
		*/
		Dispatcher() = default;
		/**
		* @brief listeners
		* @details Lista de Observers que estan escuchando mensajes
		*/
		map< string, list< Observer* > > listeners;

	public: 
		
		/**
		* @brief get_instance
		* @details Devuelve un puntero a la instancia �nica de dispatcher
		* Si ya hay una instancia creada, devuelve un puntero a esa instancia,
		* si no la hay, crea una. 
		*/
		static Dispatcher* get_instance(); 

		/**
		* @brief register_listener
		* @details Devuelve un puntero a la instancia �nica de dispatcher
		* Si ya hay una instancia creada, devuelve un puntero a esa instancia,
		* si no la hay, crea una.
		*/
		void register_listener(const string& message_id, Observer& listener); 

		/**
		* @brief unregister_listener
		* @param message_id Codigo del mensaje 
		* @param listener Observer que queremos desregistrar  
		* @details Devuelve un puntero a la instancia �nica de dispatcher
		* Si ya hay una instancia creada, devuelve un puntero a esa instancia,
		* si no la hay, crea una.
		*/
		void unregister_listener(const string& message_id, Observer& listener); 

		/**
		* @brief multicast
		* @param message Codigo del mensaje a enviar 
		* @details Envia el mensaje a todos los observers que lo esten escuchando
		*/
		void multicast(const Message& message); 


	};
}