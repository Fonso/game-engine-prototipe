/*
	Started by Alfonso L�pez Viejo on dicember of 2019
	Author: Alfonso L�pez Viejo
	Copyright � 2020 Alfonso L�pez Viejo
	Last Update: January/2020
	email: alfonsolopezviejo@gmail.com
*/

#pragma once
#include<cstdint>
#include <internal/declarations.hpp>

namespace engine
{
	/**
	*   @brief Music class
	*   @details Esta clase se encarga de reproducir una cancion 
	*/
	class Music
	{

	private: 
		Mix_Music *music = nullptr;
		int _frecuency, _channels, _samples; 
 
	public: 
		/** 
		*	@brief Music
		*	@param frecuency frecuencia del sonido
		*	@param channels numero de canales
		*	@param samples numero de muestras del sonido
		*	@details Constructor de la clase music 
		*/
		Music(int frecuency, std::uint8_t channels, std::uint16_t samples); 
		/** 
		*	@brief is_audio_init
		*	@details Comprueba si el audio de sdl esta inicializado y por lo tanto si 
		*	funciona
		*/
		bool is_audio_init();
		/** 
		*	@brief load_and_play
		*	@param path ruta de la cancion que queremos reproducir 
		*	@param loops Si queremos que la cancion loopee o no
		*	@details Permite cargar una cancion y reproducirla en un mismo paso
		*/
		void load_and_play(const char* path, int loops); 
		/** 
		*	@brief change_volume 
		*	@param new_vol nuevo volumen de la cancion
		*	@details Permite cambiar el volumen de la cancion 
		*/
		void change_volume(int new_vol);
		/**
		*	@brief FadeInMusic
		*	@param loops Si queremos que la cancion loopee o no
		*	@param ms Define la velocidad a la que queremos que se produzca el fade in
		*	@details Reproduce una cancion con una efecto de "Fade in"
		*/
		void FadeInMusic(int loops, int ms); 
		/** 
		*	@brief FadeOutMusic
		*	@param ms Define la velocidad a la que queremos que se produzca el fade in
		*	@details Deja de reproducir la musica quitandola con una efecto de "Fade out"
		*/
		void FadeOutMusic(int ms); 
		/** 
		*	@brief load_audio
		*	@param path ruta de la canci�n que queremos reproducir
		*	@details Carga un audio dado un path que es donde se encuentra
		*/
		void load_audio(const char* path); 
		/** 
		*	@brief play
		*	@param loops Si queremos que la cancion loopee o no
		*	@details Reproduce un audio determinando con loop o no  
		*/
		void play(int loops); 
		/** 
		*	@brief pause
		*	@details Pausa el audio que actualmente este sonando 
		*/
		void pause(); 
		/** 
		*	@brief resume
		*	@details Vuelve a reproducir el audio si antes habia sido pausado 
		*/
		void resume(); 
	};
}