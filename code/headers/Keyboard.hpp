/*
    Started by Alfonso López Viejo on dicember of 2019
    Author: Alfonso López Viejo
    Copyright © 2020 Alfonso López Viejo
    Last Update: January/2020
    email: alfonsolopezviejo@gmail.com
*/

#pragma once
#include <InputDevice.hpp>

namespace engine
{
    /**
    *   @brief Keyboard class  
    *   @details Esta clase se encarga de guardar todas las posibles teclas que queremos 
    *   identificar del teclado y de traducir las teclas de SDL a nuestro propio codigo de teclas
    */
    class Keyboard:InputDevice
    {
    public:
       /**
       *   @brief Key_Code
       *   @details Transforma las teclas del teclado a codigos propios
       */
        enum Key_Code
        {
            KEY_UNKOWN,

            KEY_RETURN,
            KEY_ESCAPE,
            KEY_BACKSPACE,
            KEY_TAB,
            KEY_SPACE,

            KEY_A, KEY_B, KEY_C, KEY_D, KEY_E,
            KEY_F, KEY_G, KEY_H, KEY_I, KEY_J,
            KEY_K, KEY_L, KEY_M, KEY_N, KEY_O,
            KEY_P, KEY_Q, KEY_R, KEY_S, KEY_T,
            KEY_U, KEY_V, KEY_W, KEY_X, KEY_Y,
            KEY_Z,

            KEY_0,
            KEY_1,
            KEY_2,
            KEY_3,
            KEY_4,
            KEY_5,
            KEY_6,
            KEY_7,
            KEY_8,
            KEY_9,

            KEY_UP, KEY_DOWN, KEY_RIGHT, KEY_LEFT
        };

    public:

        /** 
        *   @brief translate_sdl_key_code
        *   @details Esta función sirve para traducir un código de tecla de SDL a uno propio del engine.
        */
        static Key_Code translate_sdl_key_code (int sdl_key_code);

    };

}
