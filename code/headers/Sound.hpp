/*
	Started by Alfonso L�pez Viejo on dicember of 2019
	Author: Alfonso L�pez Viejo
	Copyright � 2020 Alfonso L�pez Viejo
	Last Update: January/2020
	email: alfonsolopezviejo@gmail.com
*/

#pragma once
#include<cstdint>
#include <internal/declarations.hpp>

typedef struct _Mix_Music Mix_Music;
namespace engine
{
	/**
	*   @brief Sound class
	*   @details Esta clase se encarga de reproducir un sonido
	*/
	class Sound
	{
	private:
		Mix_Chunk* sound = nullptr;
		int _channels, _frecuency, _samples;
	public:
		/**
		*	@brief Sound
		*	@param frecuency frecuencia del sonido 
		*	@param channels numero de canales 
		*	@param samples numero de muestras del sonido
		*	@details Constructor de la clase sound
		*/
		Sound(int frecuency, std::uint8_t channels, std::uint16_t samples);
		/**
		*	@brief load_and_play
		*	@param path ruta del sonido que queremos reproducir
		*	@param loops Si queremos que la cancion loopee o no
		*	@details Permite cargar una cancion y reproducirla en un mismo paso
		*/
		void load_and_play(const char* path, int loops, int channel);
		/**
		*	@brief play
		*	@param loops Si queremos que la cancion loopee o no
		*	@details Reproduce un audio determinando con loop o no
		*/
		void play(int loops, int channel);
		/**
		*	@brief Change_Volume_Chunk
		*	@param volume nuevo volumen del sonido
		*	@details Permite cambiar el volumen de los sonidos
		*/
		void Change_Volume_Chunk(int volume);
		/**
		*	@brief FadeInMusic
		*	@param channel Canal donde la queremos reproducir
		*	@param loops Si queremos que la cancion loopee o no
		*	@param ms Define la velocidad a la que queremos que se produzca el fade in
		*	@details Reproduce una cancion con una efecto de "Fade in"
		*/
		void FadeInSound(int channel, int loops, int ms, int ticks);
		/**
		*	@brief FadeOutMusic
		*	@param ms Define la velocidad a la que queremos que se produzca el fade in
		*	@details Deja de reproducir la musica quitandola con una efecto de "Fade out"
		*/
		void FadeOutSound(int ms);
		/**
		*	@brief pause
		*	@details Pausa el audio que actualmente este sonando
		*/
		virtual void pause();
		/**
		*	@brief resume
		*	@details Vuelve a reproducir el audio si antes habia sido pausado
		*/
		virtual void resume();
		/**
		*	@brief load_audio
		*	@param path ruta de la canci�n que queremos reproducir
		*	@details Carga un audio dado un path que es donde se encuentra
		*/
		virtual void load_audio(const char* path);
	};
}