/*
	Started by Alfonso L�pez Viejo on dicember of 2019
	Author: Alfonso L�pez Viejo
	Copyright � 2020 Alfonso L�pez Viejo
	Last Update: January/2020
	email: alfonsolopezviejo@gmail.com
*/

#pragma once
#include <Transform_Component.hpp>
#include <Entity.hpp>
#include <Sound.hpp>
#include <Script_Component.hpp>

using namespace engine; 
class Enemy : public Script_Component
{
private: 
	Transform_Component* transform;
	Transform_Component* player_transform;
	Scene *scene; 
public: 
	float speed; 
public: 
	
	Enemy(Entity *parent, float speed, Transform_Component *player_trans, Scene *scene): Script_Component(parent)
	{
		this->scene = scene; 
		transform = parent->get_transform(); 
		player_transform = player_trans;
		this->speed = speed; 
	}

	void Update()override
	{	
		Vector3 direccion{ player_transform->position.x - transform->position.x,
						   player_transform->position.y - transform->position.y,
						   player_transform->position.z - transform->position.z
					     };
		if(direccion.magnitude() < 6.f)
		{
			std::cout << " "<< "YOU LOSE" << " "; 
			scene->reset_scene(); 
		}
		transform->position = transform->position + (direccion.normalize() * speed);
	}
	
};
