/*
	Started by Alfonso L�pez Viejo on dicember of 2019
	Author: Alfonso L�pez Viejo
	Copyright � 2020 Alfonso L�pez Viejo
	Last Update: January/2020
	email: alfonsolopezviejo@gmail.com
*/

#pragma once
#include <string>
#include <map>
#include <Variant.hpp>
#include <iterator>

using namespace std;
namespace engine 
{
	class Dispatcher; 
	/**
	*   @brief Message class
	*   @details Clase encargada de crear mensajes que luego enviara el dispatcher 
	*	y seran recibidos por objetos de la clase observer
    */
	class Message
	{
	private: 
		/*
		*	@brief identificador del mensaje
		*/
		std::string id;
		/*
		*	@brief parametros que enviamos con el mensaje
		*/
		std::map< std::string, Variant > parameters;
	public:
		/*
		*	@brief Message 
		*	@param id identificador del mensaje
		*	@details Constructor de la clase Message
		*/
		Message(const std::string& id) : id(id)
		{
		}
		/*
		*	@brief get_id
		*	@details Retorna el identificador del mensaje
		*/
		std::string get_id()const{ return id; }

		/*
		*	@brief add_parameter
		*	@details A�ade un parametro al mapa de parametros
		*/
		void add_parameter(const std::string& name, const Variant& value)
		{ 
			parameters.insert(pair<string, Variant>(name, value)); 
		}

		/*
		*	@brief send_message
		*	@param m mensaje que queremos enviar
		*	@details Envia el mensaje a los observers
		*/
		void send_message(Message& m); 
	};
}