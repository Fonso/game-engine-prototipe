/*
	Started by Alfonso L�pez Viejo on dicember of 2019
	Author: Alfonso L�pez Viejo
	Copyright � 2020 Alfonso L�pez Viejo
	Last Update: January/2020
	email: alfonsolopezviejo@gmail.com
*/

#pragma once

#include <InputDevice.hpp>


namespace engine
{
	/**
	 *   @brief Mouse class
	 *   @details Esta clase se encarga de guardar todos los posibles botones de un raton 
	 *	 que queremos identificar y de traducir los botones de SDL a nuestro propio motor
	 */	
	class Mouse:InputDevice
	{
	public: 
		/**
		*   @brief Mouse_Code
		*   @details Transforma los botones del raton a codigos propios
		*/
		enum Mouse_Code
		{
			MOUSE_UNKOWN,
			MOUSE_LEFT_CLICK,
			MOUSE_RIGHT_CLICK,
			MOUSE_MIDDLE_CLICK
		};
	public: 
		/**
		*   @brief translate_sdl_mouse_code
		*   @details Esta funci�n sirve para traducir un c�digo de tecla de SDL a uno propio del engine.
		*/
		static Mouse_Code translate_sdl_mouse_code(int sdl_key_code); 
	};
}
