/*
	Started by Alfonso L�pez Viejo on dicember of 2019
	Author: Alfonso L�pez Viejo
	Copyright � 2020 Alfonso L�pez Viejo
	Last Update: January/2020
	email: alfonsolopezviejo@gmail.com
*/

#pragma once
#include <set>
#include <Timer.hpp>
#include <Task.hpp>
#include <iostream>
 
namespace engine
{
	/**
		* @brief Kernel Class
		* @details La clase se encarga de ejecutar todas las tareas dentro de su lista
		* de tareas. De esta forma, reproducimos el loop principal del motor
	*/
	class Kernel
	{	
		typedef std::multiset< Task* > Task_List; 
		/**
		*	@brief Lista de tareas
		*/
		Task_List tasks;

		/**
		*	@brief controla si debemos continuar o no el kernel
		*/
		bool exit = false;
	
	public:
		/**
		*	@brief Kernel
		*	@details Constructor de la clase Kernel 
		*/
		Kernel() 
		{
			exit = false; 
		};
		/**
		*	@brief add_task
		*	@param task Referencia de la tarea que queremos a�adir a la lista de tareas
		*	@details A�ade una nueva tarea a la lista de tareas 
		*/
		void add_task(Task& task)
		{
			tasks.insert(&task);
		}

		/**
		*	@brief execute
		*	@details Ejecuta todas las tareas en bucle hasta que la variable exit
		*   sea verdadera
		*/
		void execute(); 

		/**
		*	@brief stop
		*	@details Para la ejecucion del kernel 
		*/
		void stop()
		{
			exit = true;
		}
	};
}
