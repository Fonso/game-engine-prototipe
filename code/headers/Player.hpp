/*
	Started by Alfonso L�pez Viejo on dicember of 2019
	Author: Alfonso L�pez Viejo
	Copyright � 2020 Alfonso L�pez Viejo
	Last Update: January/2020
	email: alfonsolopezviejo@gmail.com
*/

#pragma once
#include <iostream>
#include <Sound.hpp>
#include <Script_Component.hpp>

using namespace engine; 
class Player : public Script_Component, public Observer
{
private: 
	Transform_Component* transform;
	Scene *scene;
	Sound *sound; 
	float _speed = 0.f; 
public: 
	Player(Entity *parent, Scene *s, float speed, Sound *sound) : Script_Component(parent) 
	{
		this->sound = sound; 
		_speed = speed;
		Dispatcher::get_instance()->register_listener("up",*this);
		Dispatcher::get_instance()->register_listener("right", *this);
		Dispatcher::get_instance()->register_listener("left", *this);
		Dispatcher::get_instance()->register_listener("down", *this);
		scene = s; 
		transform = parent->get_transform(); 
	};

	// Heredado v�a Observer
	virtual void handle_message(const Message& message) override;
};