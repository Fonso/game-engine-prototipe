var indexSectionsWithContent =
{
  0: "abcdefghiklmnoprstuvwxyz~",
  1: "cdeikmorstuvw",
  2: "e",
  3: "cdeikmorstuvw",
  4: "acdefghiklmnoprstuvw~",
  5: "bdfikmprstxyz",
  6: "m",
  7: "kmrt",
  8: "ckm"
};

var indexSectionNames =
{
  0: "all",
  1: "classes",
  2: "namespaces",
  3: "files",
  4: "functions",
  5: "variables",
  6: "typedefs",
  7: "enums",
  8: "enumvalues"
};

var indexSectionLabels =
{
  0: "Todo",
  1: "Clases",
  2: "Namespaces",
  3: "Archivos",
  4: "Funciones",
  5: "Variables",
  6: "typedefs",
  7: "Enumeraciones",
  8: "Valores de enumeraciones"
};

