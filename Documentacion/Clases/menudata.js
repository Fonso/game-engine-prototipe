/*
@licstart  The following is the entire license notice for the
JavaScript code in this file.

Copyright (C) 1997-2019 by Dimitri van Heesch

This program is free software; you can redistribute it and/or modify
it under the terms of version 2 of the GNU General Public License as published by
the Free Software Foundation

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

@licend  The above is the entire license notice
for the JavaScript code in this file
*/
var menudata={children:[
{text:"Página principal",url:"index.html"},
{text:"Namespaces",url:"namespaces.html",children:[
{text:"Lista de 'namespaces'",url:"namespaces.html"},
{text:"Miembros del Namespace ",url:"namespacemembers.html",children:[
{text:"Todo",url:"namespacemembers.html"},
{text:"Enumeraciones",url:"namespacemembers_enum.html"}]}]},
{text:"Clases",url:"annotated.html",children:[
{text:"Lista de clases",url:"annotated.html"},
{text:"Índice de clases",url:"classes.html"},
{text:"Jerarquía de la clase",url:"hierarchy.html"},
{text:"Miembros de las clases",url:"functions.html",children:[
{text:"Todo",url:"functions.html",children:[
{text:"a",url:"functions.html#index_a"},
{text:"b",url:"functions_b.html#index_b"},
{text:"c",url:"functions_c.html#index_c"},
{text:"d",url:"functions_d.html#index_d"},
{text:"e",url:"functions_e.html#index_e"},
{text:"f",url:"functions_f.html#index_f"},
{text:"g",url:"functions_g.html#index_g"},
{text:"h",url:"functions_h.html#index_h"},
{text:"i",url:"functions_i.html#index_i"},
{text:"k",url:"functions_k.html#index_k"},
{text:"l",url:"functions_l.html#index_l"},
{text:"m",url:"functions_m.html#index_m"},
{text:"n",url:"functions_n.html#index_n"},
{text:"o",url:"functions_o.html#index_o"},
{text:"p",url:"functions_p.html#index_p"},
{text:"r",url:"functions_r.html#index_r"},
{text:"s",url:"functions_s.html#index_s"},
{text:"t",url:"functions_t.html#index_t"},
{text:"u",url:"functions_u.html#index_u"},
{text:"v",url:"functions_v.html#index_v"},
{text:"w",url:"functions_w.html#index_w"},
{text:"x",url:"functions_x.html#index_x"},
{text:"y",url:"functions_y.html#index_y"},
{text:"z",url:"functions_z.html#index_z"},
{text:"~",url:"functions_~.html#index__7E"}]},
{text:"Funciones",url:"functions_func.html",children:[
{text:"a",url:"functions_func.html#index_a"},
{text:"c",url:"functions_func.html#index_c"},
{text:"d",url:"functions_func.html#index_d"},
{text:"e",url:"functions_func.html#index_e"},
{text:"f",url:"functions_func.html#index_f"},
{text:"g",url:"functions_func.html#index_g"},
{text:"h",url:"functions_func.html#index_h"},
{text:"i",url:"functions_func.html#index_i"},
{text:"k",url:"functions_func.html#index_k"},
{text:"l",url:"functions_func.html#index_l"},
{text:"m",url:"functions_func.html#index_m"},
{text:"n",url:"functions_func.html#index_n"},
{text:"o",url:"functions_func.html#index_o"},
{text:"p",url:"functions_func.html#index_p"},
{text:"r",url:"functions_func.html#index_r"},
{text:"s",url:"functions_func.html#index_s"},
{text:"t",url:"functions_func.html#index_t"},
{text:"u",url:"functions_func.html#index_u"},
{text:"v",url:"functions_func.html#index_v"},
{text:"w",url:"functions_func.html#index_w"},
{text:"~",url:"functions_func.html#index__7E"}]},
{text:"Variables",url:"functions_vars.html"},
{text:"Enumeraciones",url:"functions_enum.html"},
{text:"Valores de enumeraciones",url:"functions_eval.html",children:[
{text:"c",url:"functions_eval.html#index_c"},
{text:"k",url:"functions_eval.html#index_k"},
{text:"m",url:"functions_eval.html#index_m"}]}]}]},
{text:"Archivos",url:"files.html",children:[
{text:"Lista de archivos",url:"files.html"},
{text:"Miembros de los ficheros",url:"globals.html",children:[
{text:"Todo",url:"globals.html"},
{text:"typedefs",url:"globals_type.html"}]}]}]}
